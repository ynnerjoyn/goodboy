import logo from './logo.svg';

function Footer() {
  return (
    <footer>
        <div className='container'>
            <div className='row'>
                <div className='col-xl-3 mx-auto mb-4'>
                    <h2>
                        <img src={logo} alt='dog logo' />Good boy
                    </h2>
                </div>
                <div className='col-xl-2 mx-auto mb-4'>
                    <h6 className='fw-bold mb-4'>
                        Nadácia Good boy
                    </h6>
                    <p>
                        <a href='#'>O projekte</a>
                    </p>
                    <p>
                        <a href='#'>Ako na to</a>
                    </p>
                    <p>
                        <a href='#'>Kontakt</a>
                    </p>
                </div>
                <div className='col-xl-2 mx-auto mb-4'>
                    <h6 className='fw-bold mb-4'>
                        Nadácia Good boy
                    </h6>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br />Vivamus in interdum ipsum, sit amet.
                    </p>
                </div>
                <div className='col-xl-2 mx-auto mb-md-0 mb-4'>
                    <h6 className='fw-bold mb-4'>
                        Nadácia Good boy
                    </h6>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br />Vivamus in interdum ipsum, sit amet.
                    </p>
                </div>
            </div>
        </div>
    </footer>
  );
}

export default Footer;
