function Navbar() {
  return (
    <nav>
		<div>
	      <span className='fw-bold'>Nadácia Good Boy</span>
	    </div>
	    <div>
			<a href='#'>
				<i className='fa fa-facebook'></i>
			</a>
			<a href='#'>
				<i className='fa fa-instagram'></i>
			</a>
	    </div>
	</nav>
  );
}

export default Navbar;
